package ru.tsc.felofyanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IConnectionProvider {

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();
}
