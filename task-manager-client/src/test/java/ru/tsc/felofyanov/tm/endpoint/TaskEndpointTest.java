package ru.tsc.felofyanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.felofyanov.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;
import ru.tsc.felofyanov.tm.enumerated.Status;

import java.util.Date;
import java.util.List;

public class TaskEndpointTest {

    @NotNull
    private final ITaskEndpoint endpoint = ITaskEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final IProjectTaskEndpoint bindEndpoint = IProjectTaskEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String token;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse =
                authEndpoint.login(new UserLoginRequest("test", "test"));
        token = loginResponse.getToken();
        endpoint.createTask(new TaskCreateRequest(
                token, "BEFORE", "test", null, null
        ));
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertThrows(
                Exception.class, () -> endpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest())
        );
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest("qweeq", null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(token, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(token, "", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(token, "123", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(token, "123", Status.NOT_STARTED)
        ));

        @Nullable TaskCreateResponse createResponse =
                endpoint.createTask(new TaskCreateRequest(
                        token, "changeTaskStatusById", "test", null, null
                ));
        Assert.assertNotNull(createResponse);

        @Nullable final TaskDTO task = createResponse.getTask();
        Assert.assertNotNull(task);

        @Nullable TaskChangeStatusByIdResponse response =
                endpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(
                        token, task.getId(), Status.IN_PROGRESS
                ));
        Assert.assertNotNull(response);
    }

    @Test
    public void changeTaskStatusByIndex() {
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusByIndex(
                new TaskChangeStatusByIndexRequest()
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusByIndex(
                new TaskChangeStatusByIndexRequest(null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusByIndex(
                new TaskChangeStatusByIndexRequest("qweeq", null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusByIndex(
                new TaskChangeStatusByIndexRequest(token, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusByIndex(
                new TaskChangeStatusByIndexRequest(token, -1, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.changeTaskStatusByIndex(
                new TaskChangeStatusByIndexRequest(token, 1, null)
        ));

        @Nullable TaskCreateResponse createResponse =
                endpoint.createTask(new TaskCreateRequest(
                        token, "changeTaskStatusByIndex", "test", null, null
                ));
        Assert.assertNotNull(createResponse);

        @Nullable final TaskDTO task = createResponse.getTask();
        Assert.assertNotNull(task);

        @Nullable TaskChangeStatusByIndexResponse response =
                endpoint.changeTaskStatusByIndex(new TaskChangeStatusByIndexRequest(
                        token, 0, Status.IN_PROGRESS
                ));
        Assert.assertNotNull(response);
    }

    @Test
    public void clearTask() {
        Assert.assertThrows(Exception.class, () -> endpoint.clearTask(new TaskClearRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.clearTask(new TaskClearRequest(null)));
        Assert.assertThrows(Exception.class, () -> endpoint.clearTask(new TaskClearRequest("12333")));

        @Nullable TaskClearResponse response = endpoint.clearTask(new TaskClearRequest(token));
        Assert.assertNotNull(response);
        Assert.assertNull(response.getTask());
    }


    @Test
    public void createTask() {
        Assert.assertThrows(Exception.class, () -> endpoint.createTask(new TaskCreateRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.createTask(
                new TaskCreateRequest(null, "test", "test", new Date(), new Date())
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.createTask(
                new TaskCreateRequest(token, null, "test", new Date(), new Date())
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.createTask(
                new TaskCreateRequest(token, "test", null, new Date(), new Date())
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.createTask(
                new TaskCreateRequest("test", "test", "test", new Date(), new Date())
        ));

        @Nullable TaskCreateResponse createResponse =
                endpoint.createTask(new TaskCreateRequest(
                        token, "createTask", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());
    }

    @Test
    public void getTaskById() {
        Assert.assertThrows(Exception.class, () -> endpoint.getTaskById(new TaskGetByIdRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.getTaskById(
                new TaskGetByIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.getTaskById(
                new TaskGetByIdRequest("qweee", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.getTaskById(new TaskGetByIdRequest(token, null)));
        Assert.assertThrows(Exception.class, () -> endpoint.getTaskById(new TaskGetByIdRequest(token, "")));

        @Nullable TaskCreateResponse createResponse =
                endpoint.createTask(new TaskCreateRequest(
                        token, "getTaskById", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());

        TaskGetByIdResponse responseNull = endpoint.getTaskById(new TaskGetByIdRequest(token, "1qwe1"));
        Assert.assertNotNull(responseNull);
        Assert.assertNull(responseNull.getTask());

        @Nullable TaskDTO task = createResponse.getTask();
        Assert.assertNotNull(task);

        TaskGetByIdResponse response = endpoint.getTaskById(new TaskGetByIdRequest(token, task.getId()));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
    }


    @Test
    public void getTaskByIndex() {
        Assert.assertThrows(Exception.class, () -> endpoint.getTaskByIndex(new TaskGetByIndexRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.getTaskByIndex(
                new TaskGetByIndexRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.getTaskByIndex(
                new TaskGetByIndexRequest("qweee", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.getTaskByIndex(
                new TaskGetByIndexRequest(token, -1)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.getTaskByIndex(
                new TaskGetByIndexRequest(token, 20)
        ));

        TaskGetByIndexResponse response = endpoint.getTaskByIndex(new TaskGetByIndexRequest(token, 1));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
    }

    @Test
    public void listTask() {
        Assert.assertThrows(Exception.class, () -> endpoint.listTask(new TaskListRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.listTask(new TaskListRequest(null, null)));
        Assert.assertThrows(Exception.class, () -> endpoint.listTask(new TaskListRequest("", null)));
        Assert.assertThrows(Exception.class, () -> endpoint.listTask(new TaskListRequest("qwe", null)));

        TaskListResponse response = endpoint.listTask(new TaskListRequest(token, null));
        Assert.assertNotNull(response);
        endpoint.clearTask(new TaskClearRequest(token));

        TaskListResponse responseNull = endpoint.listTask(new TaskListRequest(token, null));
        Assert.assertNotNull(responseNull);
        Assert.assertNull(responseNull.getTasks());
    }

    @Test
    public void removeTaskById() {
        Assert.assertThrows(Exception.class, () -> endpoint.removeTaskById(new TaskRemoveByIdRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.removeTaskById(
                new TaskRemoveByIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeTaskById(
                new TaskRemoveByIdRequest("", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeTaskById(
                new TaskRemoveByIdRequest("123", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeTaskById(
                new TaskRemoveByIdRequest(token, null)
        ));

        @Nullable TaskCreateResponse createResponse =
                endpoint.createTask(new TaskCreateRequest(
                        token, "removeTaskById", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());

        @Nullable TaskDTO task = createResponse.getTask();

        TaskRemoveByIdResponse response =
                endpoint.removeTaskById(new TaskRemoveByIdRequest(token, task.getId()));
        Assert.assertNotNull(response);

        TaskGetByIdResponse responseTest = endpoint.getTaskById(new TaskGetByIdRequest(token, task.getId()));
        Assert.assertNotNull(responseTest);
        Assert.assertNull(responseTest.getTask());
    }


    @Test
    public void removeTaskByIndex() {
        Assert.assertThrows(Exception.class, () -> endpoint.removeTaskByIndex(new TaskRemoveByIndexRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.removeTaskByIndex(
                new TaskRemoveByIndexRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeTaskByIndex(
                new TaskRemoveByIndexRequest("", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeTaskByIndex(
                new TaskRemoveByIndexRequest("123", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeTaskByIndex(
                new TaskRemoveByIndexRequest(token, -1)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.removeTaskByIndex(
                new TaskRemoveByIndexRequest(token, 100)
        ));

        @Nullable TaskCreateResponse createResponse =
                endpoint.createTask(new TaskCreateRequest(
                        token, "removeTaskByIndex", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());

        @Nullable TaskDTO createTask = createResponse.getTask();

        TaskListResponse listTask = endpoint.listTask(new TaskListRequest(token, null));
        Assert.assertNotNull(listTask.getTasks());
        @Nullable List<TaskDTO> indexTask = listTask.getTasks();

        TaskRemoveByIndexResponse response =
                endpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(token, indexTask.size() - 1));
        Assert.assertNotNull(response);

        TaskGetByIdResponse responseTest =
                endpoint.getTaskById(new TaskGetByIdRequest(token, createTask.getId()));
        Assert.assertNotNull(responseTest);
        Assert.assertNull(responseTest.getTask());
    }

    @Test
    public void updateTaskById() {
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskById(new TaskUpdateByIdRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskById(
                new TaskUpdateByIdRequest(null, "test", "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskById(
                new TaskUpdateByIdRequest(token, null, "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskById(
                new TaskUpdateByIdRequest(token, "test", null, "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskById(
                new TaskUpdateByIdRequest(token, "test", "test", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskById(
                new TaskUpdateByIdRequest("test", "test", "test", "test")
        ));

        @Nullable TaskCreateResponse createResponse =
                endpoint.createTask(new TaskCreateRequest(
                        token, "updateTaskById", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());

        @Nullable TaskDTO task = createResponse.getTask();
        Assert.assertNotNull(task.getId());

        TaskUpdateByIdResponse response =
                endpoint.updateTaskById(new TaskUpdateByIdRequest(
                        token, task.getId(), "afterUpdate", "updateTaskById"
                ));
        Assert.assertNotNull(response.getTask());
    }

    @Test
    public void updateTaskByIndex() {
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskByIndex(new TaskUpdateByIndexRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskByIndex(
                new TaskUpdateByIndexRequest(null, 0, "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskByIndex(
                new TaskUpdateByIndexRequest(token, null, "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskByIndex(
                new TaskUpdateByIndexRequest(token, 0, null, "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskByIndex(
                new TaskUpdateByIndexRequest(token, 0, "test", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskByIndex(
                new TaskUpdateByIndexRequest("test", 0, "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskByIndex(
                new TaskUpdateByIndexRequest("test", -1, "test", "test")
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.updateTaskByIndex(
                new TaskUpdateByIndexRequest("test", 100, "test", "test")
        ));

        @Nullable TaskCreateResponse createResponse =
                endpoint.createTask(new TaskCreateRequest(
                        token, "updateTaskByIndex", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());

        TaskListResponse listTask = endpoint.listTask(new TaskListRequest(token, null));
        Assert.assertNotNull(listTask.getTasks());
        @Nullable List<TaskDTO> indexTask = listTask.getTasks();

        TaskUpdateByIndexResponse response =
                endpoint.updateTaskByIndex(new TaskUpdateByIndexRequest(
                        token, indexTask.size() - 1, "afterUpdate", "updateTaskByIndex"
                ));
        Assert.assertNotNull(response.getTask());
    }

    @Test
    public void listTaskByProjectId() {
        Assert.assertThrows(Exception.class, () -> endpoint.listTaskByProjectId(new TaskListByProjectIdRequest()));
        Assert.assertThrows(Exception.class, () -> endpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest("", null)
        ));
        Assert.assertThrows(Exception.class, () -> endpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest(token, null)
        ));

        ProjectGetByIndexResponse projectTest =
                projectEndpoint.getProjectByIndex(new ProjectGetByIndexRequest(token, 0));
        Assert.assertNotNull(projectTest.getProject());
        @Nullable ProjectDTO project = projectTest.getProject();

        @Nullable TaskCreateResponse createResponse =
                endpoint.createTask(new TaskCreateRequest(
                        token, "listTaskByProjectId", "test", null, null
                ));
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());

        @Nullable TaskDTO task = createResponse.getTask();

        ProjectBindTaskByIdResponse bindTask =
                bindEndpoint.bindTaskToProjectId(new ProjectBindTaskByIdRequest(token, project.getId(), task.getId()));
        Assert.assertNotNull(bindTask);

        TaskListByProjectIdResponse response =
                endpoint.listTaskByProjectId(new TaskListByProjectIdRequest(token, project.getId()));
        Assert.assertNotNull(response.getTasks());
    }
}
