package ru.tsc.felofyanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.TaskRemoveByIdRequest;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by id.";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken(), id);
        getTaskEndpoint().removeTaskById(request);
    }
}
