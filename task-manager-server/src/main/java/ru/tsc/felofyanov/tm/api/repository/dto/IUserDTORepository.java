package ru.tsc.felofyanov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;

public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @Nullable
    UserDTO findFirstByLogin(@Nullable String login);

    @Nullable
    UserDTO findFirstByEmail(@Nullable String email);

    @Nullable
    @Transactional
    UserDTO deleteByLogin(@Nullable String login);
}
