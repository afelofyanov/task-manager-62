package ru.tsc.felofyanov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.felofyanov.tm.api.repository.dto.IUserOwnerDTORepository;
import ru.tsc.felofyanov.tm.api.service.dto.IUserOwnerDTOService;
import ru.tsc.felofyanov.tm.dto.model.AbstractWbsDTO;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.entity.ModelEmptyException;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.util.GenericUtil;

import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractUserOwnedDTOService<M extends AbstractWbsDTO, R extends IUserOwnerDTORepository<M>>
        extends AbstractDTOService<M, R> implements IUserOwnerDTOService<M> {

    @Nullable
    protected Class<M> clazz = getModelClass();

    @NotNull
    @Autowired
    protected IUserDTORepository userRepository;

    @NotNull
    protected abstract IUserOwnerDTORepository<M> getRepository();

    @NotNull
    @SneakyThrows
    protected Class<M> getModelClass() {
        return (Class<M>) GenericUtil.getClassFirst(getClass());
    }

    @Nullable
    protected M getResult(@NotNull final Page<M> page) {
        if (page.stream().count() == 0) return null;
        return page.getContent().get(0);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        @NotNull final Optional<UserDTO> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        @Nullable final M model = clazz.newInstance();
        model.setName(name);
        model.setUserId(userId);
        return repository.save(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable String userId, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        @NotNull final Optional<UserDTO> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        @Nullable final M model = clazz.newInstance();
        model.setName(name);
        model.setDescription(description);
        model.setUserId(userId);
        return repository.save(model);
    }

    @NotNull
    @Override
    public List<M> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @Override
    public void clearByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existsByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneByIdUserId(userId, id) != null;
    }

    @Nullable
    @Override
    public M findOneByIdUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndexByUserId(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        return getResult(repository.findAllByUserId(userId, PageRequest.of(index, 1)));
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        @Nullable final M result = repository.findFirstByUserIdAndId(userId, model.getId());
        if (result == null) throw new ModelEmptyException();
        repository.delete(result);
        return result;
    }

    @Nullable
    @Override
    public M removeByIdByUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        @Nullable final M result = repository.deleteByUserIdAndId(userId, id);
        if (result == null) throw new ModelEmptyException();
        return result;
    }

    @Nullable
    @Override
    public M removeByIndexByUserId(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();

        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        @Nullable final M result = findOneByIndexByUserId(userId, index);
        if (result == null) throw new ModelEmptyException();
        repository.delete(result);
        return result;
    }

    @Override
    public M update(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        return repository.save(model);
    }

    @NotNull
    @Override
    public M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        @Nullable final M result;
        result = repository.findFirstByUserIdAndId(userId, id);
        if (result == null) throw new ModelNotFoundException();

        result.setName(name);
        result.setDescription(description);
        return repository.save(result);
    }

    @NotNull
    @Override
    public M updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        @Nullable final M result;
        result = findOneByIndexByUserId(userId, index);
        if (result == null) throw new ModelNotFoundException();
        result.setName(name);
        result.setDescription(description);
        return repository.save(result);
    }

    @NotNull
    @Override
    public M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        @Nullable final M result;

        result = repository.findFirstByUserIdAndId(userId, id);
        if (result == null) throw new ModelNotFoundException();
        result.setStatus(status);
        return repository.save(result);
    }

    @NotNull
    @Override
    public M changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();

        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        @Nullable final M result;

        result = findOneByIndexByUserId(userId, index);
        if (result == null) throw new ModelNotFoundException();
        result.setStatus(status);
        return repository.save(result);
    }

    @Override
    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnerDTORepository<M> repository = getRepository();
        return repository.countByUserId(userId);
    }
}
